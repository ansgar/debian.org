#!/bin/bash

# script to update apache site configs
#
# this script is designed to be run under sudo by
# the maintainers of our various apache2 vhosts.
#
# It checks the validity of the current apache config and
# then updates one vhost config from the staging area that
# is writeable by the user.
#
# This script ensures that the resulting apache config is then
# still syntactically valid and reloads apache.  If the new config
# is not OK then the change is reverted.
#
# All config changes are tracked in RCS.
#
# needs something like this in sudoers:
# %apachectrl	ALL=(root)	/usr/sbin/apache2-vhost-update


# Copyright (c) 2009,2010 Peter Palfrader <peter@palfrader.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



STAGINGDIR="/etc/apache2/sites-staging"
DESTINATION="/etc/apache2/sites-available"

usage() {
	bn="`basename "$0"`"
	echo "Usage: $bn -r|--reload"
	echo "       $bn <site name>"
	echo "       $bn -h|--help"
	echo
	echo "-r ...            only reload apache"
	echo "-h ..             print this text"
	echo
	echo "With no options copy site's config from etc/apache2/staging"
	echo "to live config and reload."
}

if [ "$#" != 1 ]; then
	usage >&2
	exit 1
fi
if [ "$1" = "-h" ] || [ "$1" == "--help" ]; then
	usage
	exit 0
fi

if ! [ -x /usr/sbin/apache2ctl ]; then
	echo "Ehhh, there is no Apache2 on this machine" >&2
	exit 1
fi

# If the apache config is already broken, then don't do anything at all
/usr/sbin/apache2ctl configtest
if [ "$?" != "0" ]; then
	echo "configtest returned errors already; not doing anything." >&2
	exit 1
fi

if [ "$1" = "-r" ] || [ "$1" == "--reload" ]; then
	env -i /etc/init.d/apache2 reload
	exit 0
fi


site="$1"
if echo "$site" | grep /; then
	echo "Illegal site name" >&2
	exit 1
fi

if [ -f "$STAGINGDIR/$site.conf" -a ! -f "$STAGINGDIR/$site" ] ; then
	site="$site.conf"
fi

if ! [ -f "$STAGINGDIR/$site" ]; then
	echo "$STAGINGDIR/$site does not exist or is not a file" >&2
	exit 1
fi

rcsdiff -q "$DESTINATION/$site" > /dev/null
if [ "$?" != "0" ]; then
	echo "RCS reported uncommitted differences in live version; aborting." >&2
	exit 1
fi

if diff "$STAGINGDIR/$site" "$DESTINATION/$site" > /dev/null; then
	echo "No differences, not doing anything." >&2
	exit 1
fi

co -l "$DESTINATION/$site" < /dev/null
if [ "$?" != "0" ]; then
	echo "Could not get a lock/checkout $DESTINATION/$site." >&2
	exit 1
fi

cp -f "$STAGINGDIR/$site" "$DESTINATION/$site"

if grep -i include "$DESTINATION/$site" > /dev/null; then
	echo "New site may have include statements - rejecting." >&2
	rm -f "$DESTINATION/$site"
	co "$DESTINATION/$site"
	exit 1
fi

if grep -i LoadModule "$DESTINATION/$site" > /dev/null; then
	echo "New site may have LoadModule statements - rejecting." >&2
	rm -f "$DESTINATION/$site"
	co "$DESTINATION/$site"
	exit 1
fi

if grep -i LoadFile "$DESTINATION/$site" > /dev/null; then
	echo "New site may have LoadFile statements - rejecting." >&2
	rm -f "$DESTINATION/$site"
	co "$DESTINATION/$site"
	exit 1
fi

/usr/sbin/apache2ctl configtest
if [ "$?" != "0" ]; then
	echo "configtest returned errors; reverting." >&2
	rm -f "$DESTINATION/$site"
	co "$DESTINATION/$site"
	exit 1
fi

echo "Differences:"
rcsdiff -u "$DESTINATION/$site"

echo "update run by $USER($SUDO_USER) at `date -R -u` on `hostname -f`" | \
	ci -u "$DESTINATION/$site"
if [ "$?" != "0" ]; then
	echo "Commit failed?" >&2
	exit 1
fi

env -i /etc/init.d/apache2 reload
